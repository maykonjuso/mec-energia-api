import pytest
from unittest.mock import patch
from utils.email.send_email import send_email

@pytest.mark.parametrize(
    "smtp_exception, starttls_exception, login_exception, sendmail_exception, expected_result",
    [
        (None, None, None, None, None),  # Caso 1: Envio de e-mail bem-sucedido
        (Exception("SMTP Connection Error"), None, None, None, False),  # Caso 2: Falha na criação da instância de smtplib.SMTP
        (None, Exception("StartTLS Error"), None, None, False),  # Caso 3: Falha na chamada s.starttls()
        (None, None, Exception("Login Error"), None, False),  # Caso 4: Falha no login no servidor SMTP
        (None, None, None, Exception("Sendmail Error"), False),  # Caso 5: Falha no envio de e-mail
    ],
)
def test_send_email_exceptions(
    smtp_exception, starttls_exception, login_exception, sendmail_exception, expected_result
):
    with patch("smtplib.SMTP", side_effect=smtp_exception), \
         patch("smtplib.SMTP.starttls", side_effect=starttls_exception), \
         patch("smtplib.SMTP.login", side_effect=login_exception), \
         patch("smtplib.SMTP.sendmail", side_effect=sendmail_exception):

        try:
            result = send_email(
                sender_email="unb@gmail.com",
                sender_password="senha",
                recipient_email="fga@gmail.com",
                title="titulo teste 1",
                text_body="conteudo teste 1",
            )
            assert result == expected_result
        except Exception as error:
            if expected_result:
                # Se a exceção não era esperada, relançamos a exceção
                raise
            assert str(error) == f'Error Send Email: {str(smtp_exception)}' if smtp_exception else 'Error Send Email: Unknown Exception'
