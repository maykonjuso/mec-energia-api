import pytest
from unittest.mock import patch
from tariffs.models import Distributor, Tariff
from unittest.mock import MagicMock

def test_remove_tariffs_by_subgroups_exception():
    # Arrange
    distributor_instance = Distributor()
    distributor_instance.id = 1
    request_subgroup = 'some_subgroup'

    # Mockando o método Tariff.objects.get
    with patch('tariffs.models.Tariff.objects.get') as mock_get:
        # Configurando o retorno do método para simular objetos existentes
        mock_get.side_effect = [MagicMock(), MagicMock()]

        # Executando a função
        result = distributor_instance.remove_tariffs_by_subgroups(request_subgroup)

    # Verificando se a função retornou a mensagem esperada
    assert result == 'Tariffs by subgroups removed sucessfully'
